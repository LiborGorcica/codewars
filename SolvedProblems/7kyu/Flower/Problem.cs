namespace myjinxin
{
    using System;
    
    public class Kata
    {
        public int GrowingPlant(int UpSpeed, int DownSpeed, int DesiredHeight)
        {
          if(UpSpeed < 5 || UpSpeed > 100)
            throw new ArgumentException($"{nameof(UpSpeed)} Constraints: 5 ≤ upSpeed ≤ 100.");
          
          if(DownSpeed >= UpSpeed || DownSpeed < 2)
            throw new ArgumentException($"{nameof(DownSpeed)} Constraints: 2 ≤ downSpeed < upSpeed.");
        
          if(DesiredHeight < 4 || DesiredHeight > 1000)
            throw new ArgumentException($"{nameof(DesiredHeight)} Constraints: 4 ≤ desiredHeight ≤ 1000.");

          if(UpSpeed > DesiredHeight)
            return 1;

          int an = DesiredHeight;
          int a0 = UpSpeed;
          int step = UpSpeed - DownSpeed;
          
          var estimatedDays = (an + step - a0) / step;
          Console.WriteLine(estimatedDays);
          var nthNumber = a0 + ((estimatedDays-1) * step);
          Console.WriteLine(nthNumber);
          return nthNumber >= DesiredHeight 
            ? estimatedDays
            : estimatedDays + 1;
        }
    }
}

namespace myjinxin
{
    using NUnit.Framework;
    using System;
    
    [TestFixture]
    public class myjinxin
    {
        
        [Test]
        public void BasicTests(){
            var kata=new Kata();
            
            Assert.AreEqual(10,kata.GrowingPlant(100,10,910));
            Assert.AreEqual(1,kata.GrowingPlant(10,9,4));
            
            
            Assert.AreEqual(6,kata.GrowingPlant(41, 4, 195));
            Assert.AreEqual(20,kata.GrowingPlant(66, 29, 755));
          }
                 
    }
}