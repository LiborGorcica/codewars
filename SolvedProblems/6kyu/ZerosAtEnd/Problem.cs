namespace current
{
    public class Kata
    {
        public static int[] MoveZeroes(int[] arr)
        {
            var arr2 = new int[arr.Length];
            var amountOfZeros = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if(arr[i] == 0) {
                    arr2[arr.Length - 1 - amountOfZeros] = arr[i];
                    amountOfZeros ++;
                } else {
                    arr2[i - amountOfZeros] = arr[i];
                }
            }

            return arr2;
        }
    }
    namespace Solution
    {
        using NUnit.Framework;
        using System;

        [TestFixture]
        public class Sample_Test
        {
            [Test]
            public void Test()
            {
                Assert.AreEqual(new int[] { 1, 2, 1, 1, 3, 1, 0, 0, 0, 0 }, Kata.MoveZeroes(new int[] { 1, 2, 0, 1, 0, 1, 0, 3, 0, 1 }));
            }
        }
    }
}