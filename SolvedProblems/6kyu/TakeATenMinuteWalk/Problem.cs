namespace TenMinuteWalk {
  public class Kata
  {
    // Condition 1. the walk must be exactly 10 minutes (10 steps)
    // Condition 2. return to the begining

    public static bool IsValidWalk(string[] walk)
    {
      if(walk.Length != 10) {
        return false;
      }
      
      int verticalCount = 0;
      int horizontalCount = 0;
      foreach(var d in walk)
      {
        switch(d) 
        {
          case "n": verticalCount ++; break;
          case "s": verticalCount --; break;
          case "w": horizontalCount ++; break;
          case "e": horizontalCount --; break;
        }
      }
      
      return verticalCount == 0 && horizontalCount == 0;
    }
  }

  namespace Solution 
  {
    using NUnit.Framework;
    using System;

    [TestFixture]
    public class SolutionTest
    {
      [Test]
      public void SampleTest()
      {
        Assert.AreEqual(true, Kata.IsValidWalk(new string[] {"n","s","n","s","n","s","n","s","n","s"}), "should return true");
        Assert.AreEqual(false, Kata.IsValidWalk(new string[] {"w","e","w","e","w","e","w","e","w","e","w","e"}), "should return false");
        Assert.AreEqual(false, Kata.IsValidWalk(new string[] {"w"}), "should return false");
        Assert.AreEqual(false, Kata.IsValidWalk(new string[] {"n","n","n","s","n","s","n","s","n","s"}), "should return false");
      }
    }
  }
}