using System.Linq;
using NUnit.Framework;

public class Kata
{
    public static string PigIt(string str)
    {
        var words = str.Split(" ");

        var pygyfied = words
            .Select(w => IsWord(w) ? PigyfyWord(w) : w);
        
        return string.Join(" ", pygyfied);
    }

    private static string PigyfyWord(string word)
    {
        var chars = word.ToCharArray();
        var charsWithoutFirst = chars.Skip(1);
        return string.Join("", charsWithoutFirst) + chars[0] + "ay";
    }

    private static bool IsWord(string str) {
        var firstLetter = str.ToCharArray()[0];
        return  (firstLetter >= 'a' && firstLetter <= 'z') ||
                (firstLetter >= 'A' && firstLetter <= 'Z');
    }
}



[TestFixture]
public class KataTest
{
  [Test]
  public void KataTests()
  {
    Assert.AreEqual("igPay atinlay siay oolcay", Kata.PigIt("Pig latin is cool"));
    Assert.AreEqual("hisTay siay ymay tringsay", Kata.PigIt("This is my string"));
    Assert.AreEqual("hisTay siay ymay tringsay !", Kata.PigIt("This is my string !"));
  }
}