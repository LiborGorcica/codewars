using System;
using System.Collections.Generic;
using System.Linq;

namespace CaesarCipher 
{
public class Kata
{
    private class AsciRange
    {
        public int Min { get; }
        public int Max { get; }
        public int Diff => Max - Min + 1;

        public AsciRange(int min, int max)
        {
            Min = min;
            Max = max;
        }
    }
    public static string Rot13(string message)
    {
        var asciRanges = GetAsciRanges().ToList();
        var shitfted = message.ToCharArray()
          .Select(ch => Shift(ch, 13, asciRanges));

        return string.Join("", shitfted);
    }

    private static char Shift(char ch, int positions, List<AsciRange> asciRanges)
    {
        var ich = (int)ch;
        
        foreach (var asciRange in asciRanges)
        {
            if (ich < asciRange.Min || ich > asciRange.Max)
                continue;

            var shifted = (((ich - asciRange.Min) + positions) % asciRange.Diff) + asciRange.Min;
            return (char)shifted;
        }

        return ch;
    }

    private static IEnumerable<AsciRange> GetAsciRanges()
    {
        yield return new AsciRange((int)'a', (int)'z');
        yield return new AsciRange((int)'A', (int)'Z');
    }
}

namespace Solution
{
    using NUnit.Framework;
    using System;

    [TestFixture]
    public class SolutionTest
    {
        [Test, Description("test")]
        public void testTest()
        {
            Assert.AreEqual("grfg", Kata.Rot13("test"), string.Format("Input: test, Expected Output: grfg, Actual Output: {0}", Kata.Rot13("test")));
        }

        [Test, Description("Test")]
        public void TestTest()
        {
            Assert.AreEqual("Grfg", Kata.Rot13("Test"), string.Format("Input: Test, Expected Output: Grfg, Actual Output: {0}", Kata.Rot13("Test")));
        }
    }
}

}