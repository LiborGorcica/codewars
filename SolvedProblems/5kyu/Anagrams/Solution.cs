using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;


namespace Anagrams
{
    public static class Kata
    {
        public static List<string> Anagrams(string word, List<string> words)
        {
            if(words.Count == 0) {
                return Enumerable.Empty<string>().ToList();
            }

            var anagrams = GetAnagrams(word, words).ToList();
            return anagrams;
        }

        private static IEnumerable<string> GetAnagrams(string word, List<string> candidates)
        {
            var wordDict = GetLettersWithCount(word);
            
            foreach(var candidate in candidates)
            {
                if(word.Length != candidate.Length)
                    continue;

                var candidateDict = GetLettersWithCount(candidate);
                if(IsAnagram(wordDict, candidateDict))
                {
                    yield return candidate;
                }
            }
        }

        private static Dictionary<char, int> GetLettersWithCount(string word)
        {
            return word.GroupBy(x => x).ToDictionary(g => g.Key, g => g.Count(c => true));
        }

        private static bool IsAnagram(Dictionary<char, int> word, Dictionary<char, int> candidate) 
        {
            foreach(var ch in word.Keys) {
                if(!candidate.ContainsKey(ch) || candidate[ch] != word[ch]) {
                    return false;
                }
            }

            return true;
        }
    }

    [TestFixture]
    public class SolutionTest
    {
        [Test]
        public void SampleTest()
        {
            Assert.AreEqual(new List<string> { "a" }, Kata.Anagrams("a", new List<string> { "a", "b", "c", "d" }));
            Assert.AreEqual(new List<string> { "ab", "ba" }, Kata.Anagrams("ab", new List<string> { "aa", "ab", "ba", "bb" }));
            Assert.AreEqual(new List<string> { "carer", "arcre", "carre" }, Kata.Anagrams("racer", new List<string> { "carer", "arcre", "carre", "racrs", "racers", "arceer", "raccer", "carrer", "cerarr" }));
        }
    }
}